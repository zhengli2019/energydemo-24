# -> THIS FILE MIGHT NOW BE REDUNDENT (check clim-diff.py <-
''' Ensemble Statistics
    ===================
    Outline a script to calculate ensemble statistics for given 
    parameters

    The metprms considered are assigned by arguments so use for example:
    `$ python ensemble-stats.py pr`

    * Assumes a certain directory structure. Edit the `datadir` variable 
      if this doesn't fit.                                            
                                                                       '''
import matplotlib.pyplot as plt             
import matplotlib as mpl
import seaborn as sns
import xarray as xr
import pandas as pd
import glob as grod
import numpy as np
import cartopy
import cftime
import sys
import os

from cartopy.io.img_tiles import Stamen
from cdo import *
cdo = Cdo()

stamen_terrain = Stamen('terrain-background') 
stamen_toner   = Stamen('toner-background')

metprm = sys.argv[1]  #'pr'                  # use externally set fields
#time_step = sys.argv[2] #month #annual

def get_year(x) :
    '''
    Recognize the type of datetime in different simulations so can use
    the correct routines to read the years covered in the simulations.
    '''
    if type(x) == cftime._cftime.DatetimeNoLeap:
        return x.year
    elif type(x) == cftime._cftime.Datetime360Day:
        return x.year		
    else:
        return pd.to_datetime(x).year

def set_values(hsim_path,psim_path,metprm) :
    ''' 
    Read in files and calculate tables of relative differences that can 
    be passed to plotting routines. 

    Need to pick years out of what we have access to that we want to make 
    a climatology from. Should use the same number of years in both for 
    consistancy (if not need to be very careful about how significance of 
    change is computed to account for the uncertainy in the means being 
    different). 
    * Here we have hard coded the periods between 1976:2005 and 
      2021:2050. That should be adjusted if desired. 


    Variables
    ---------
    hsim_path : path to historical simulation (str)
    psim_path : path to projection simulation (str)
    metprm : name of meteorological parameter being investigated (str)
    '''

    ## Read in data
    H = xr.open_dataset(hsim_path)
    P = xr.open_dataset(psim_path)   
    
    ## Change units
    if metprm == 'pr' : 
        H.pr.values = 86400*H.pr.values
        P.pr.values = 86400*P.pr.values
        H.pr.attrs['units'] = 'mm/month'
        P.pr.attrs['units'] = 'mm/month'
    if metprm == 'tasmax':
       if H.tasmax.values.max() > 200 :
           H.tasmax.values = H.tasmax.values - 273.15
       if P.tasmax.values.max() > 200 :
           P.tasmax.values = P.tasmax.values - 273.15
       H.tasmax.attrs['units'] = 'Celsius Degree'
       P.tasmax.attrs['units'] = 'Celsius Degree'

    ## Monthly Climatologies
    idx_clim = [get_year(x)
                in range(1976,2006) 
                for x in H.time.values[:]]
    H = H.sel(time=idx_clim)

    idx_clim = [get_year(x)
                in range(2021,2051) 
                for x in P.time.values[:]]
    P = P.sel(time=idx_clim)

    nyrs = len(
        np.unique(
            [get_year(x) for x in H.time.values]
        ))

    H_clim = H.groupby('time.month').mean('time')
    H_clim['StndDev'] = H.groupby('time.month').std('time')[metprm]
    H_clim['StndErr'] = H_clim['StndDev']/np.sqrt(nyrs)

    nyrs = len(
        np.unique(
            [get_year(x) for x in P.time.values]
        ))

    P_clim = P.groupby('time.month').mean('time')
    P_clim['StndDev'] = P.groupby('time.month').std('time')[metprm]
    P_clim['StndErr'] = P_clim['StndDev']/np.sqrt(nyrs)

    ''' 
    Now calculate the relative difference between the climatology means in 
    the projection and the 'base line' values. As well, can can calculate 
    how many sigma levels we need to exapand the standard errors of those 
    means in order to get the raw differences to overlap, which here 
    functions as an addhoc probability of the differences being significant. 
    * Would be better to estimate this through bootstraping, and maybe to 
      do the same with the standard deviation values as well.
    '''

    D = H_clim.copy(deep=True)                   # create a new copy of grid  
    D = D.drop([metprm,'StndDev','StndErr'])     # make it an empty copy

    D['raw_diff'] = (                            # diff climatologies
        P_clim[metprm] - H_clim[metprm] )
    
    D['rel_diff'] = D.raw_diff/H_clim[metprm]    # diff in mean
                                                 # ... as % of historical
    
    D['dev_diff'] = (                            # diff in variation
        P_clim.StndDev - H_clim.StndDev )        # ... as % of historical
    D['rel_dev'] = D.dev_diff/H_clim.StndDev

    ''' Record how many stnd err devs between the climatology means '''
    D['sig_diff'] = D.raw_diff.copy(deep=True)   
    D['sig_diff'].values = (D.sig_diff.values*0)+4  
    for m in range(1,13) : 
        for sig_lev in range(4,0,-1) : 
            D['sig_diff'].sel(month=m).values[
                np.abs(D.raw_diff.sel(month=m)) 
                <= (sig_lev*P_clim.StndErr.sel(month=m)
                    + sig_lev*H_clim.StndErr.sel(month=m))] = sig_lev
        


    return(D)

def regrid(data,scheme,griddes_file) :
    '''
    Interpolate a given data set onto a predefined grid
    * This lets us use the CDO convince function but makes the 
      implementation clunky since it leans a lot on creating external 
      files. Would be nice to work out the math using just xarray and 
      other internal libraries. Delete the temporary files created 
      to reduce the function's external effects (even though in practice
      often end up saving the output after calling the function). 
    One hassle here is that even though immediatly load the values and 
    delete the hard copy file, the output file still has to have a 
    unique name to avoid having the differnet variables "point" towards
    the wrong data-shadow. 
    
    Variables
    ---------
    X : data set for regridding (xarray)
    scheme : cdo name for remapping routine, e.g. "nn" (str)
    griddes_file : path to file describing the desired grid (str)
    '''
    tag = pd.datetime.now().strftime('%Y%m%d%H%M%S%f')
    data.to_netcdf('tmp0.nc')
    if scheme == 'nn' :
        cdo.remapnn(griddes_file,
                  input = 'tmp0.nc', #'tmp0.nc',
                  output = 'tmp'+tag+'.nc',
                  options = '-f nc')
        
    # -> ADD OTHER FUNCTION LABELS <- 
    #    --------------------------
    #    Or, can do something fancy where call the
    #    function using its name described with a
    #    string
    
    else :
        print('+++ Unknown remapping scheme +++')
        return()

    data = xr.open_dataset('tmp'+tag+'.nc')
    [os.remove(x) for x in ['tmp0.nc','tmp'+tag+'.nc']]
    return(data)
      
    
def show_range(D,latlons,metprm) :
    '''
    Write out the range of values in the difference statistics

    Variables
    ---------
    X : data file containing difference statistics  (xarray)
    '''
    if metprm == 'tasmax':
        mean_diff = 'raw_diff'
    else:
        mean_diff = 'rel_diff' 
    if latlons:
        min_lat =  latlons[0] #D.lat.values.min()
        max_lat =  latlons[1] #D.lat.values.max()
        min_lon =  latlons[2] #D.lon.values.min()
        max_lon =  latlons[3] #D.lon.values.max()
        min_mean_diff=np.round(D[mean_diff].where((D.lat>min_lat)
				& (D.lat<max_lat)&(D.lon<max_lon)&(D.lon>min_lon),
				drop=True).min().values,decimals=2)*100
        mean_mean_diff=np.round(D[mean_diff].where((D.lat>min_lat)
				&(D.lat<max_lat)&(D.lon<max_lon)&(D.lon>min_lon),
				drop=True).mean().values,decimals=2)*100
        max_mean_diff=np.round(D[mean_diff].where((D.lat>min_lat)
				&(D.lat<max_lat)&(D.lon<max_lon)&(D.lon>min_lon),
				drop=True).max().values,decimals=2)*100
        min_stnd_dev=np.round(D.dev_diff.where((D.lat>min_lat)&
				(D.lat<max_lat)&(D.lon<max_lon)&(D.lon>min_lon)
				,drop=True).min().values,decimals=2)*100
        mean_stnd_dev=np.round(D.dev_diff.where((D.lat>min_lat)&
				(D.lat<max_lat)&(D.lon<max_lon)&(D.lon>min_lon),
				drop=True).mean().values,decimals=2)*100
        max_stnd_dev=np.round(D.dev_diff.where((D.lat>min_lat)&
				(D.lat<max_lat)&(D.lon<max_lon)&(D.lon>min_lon),
				drop=True).max().values,decimals=2)*100
    else:
        min_mean_diff=np.round(D[mean_diff].min().values,decimals=2)*100
        mean_mean_diff=np.round(D[mean_diff].mean().values,decimals=2)*100
        max_mean_diff=np.round(D[mean_diff].max().values,decimals=2)*100
        min_stnd_dev=np.round(D.dev_diff.min().values,decimals=2)*100
        mean_stnd_dev=np.round(D.dev_diff.mean().values,decimals=2)*100
        max_stnd_dev=np.round(D.dev_diff.max().values,decimals=2)*100

    print('---------------------------------------')
    print('min mean diff (as %): ',
          str(min_mean_diff),'%')
    print('mean mean diff (as %): ',
          str(mean_mean_diff),'%')
    print('max mean diff (as %): ',
          str(max_mean_diff),'%')
    print()
    print('min stnd dev (as %): ',
          str(min_stnd_dev),'%')
    print('mean stnd dev (as %): ',
          str(mean_stnd_dev),'%')
    print('max stnd dev (as %): ',
          str(max_stnd_dev),'%')
    print('---------------------------------------')
    
    return (min_mean_diff,mean_mean_diff,max_mean_diff,min_stnd_dev,mean_stnd_dev,max_stnd_dev)


def map_ensemble_values(D,latlons,metprm,show_dev=True,sim_name='') :
    '''
    Draw values on a map.

    Variables
    ---------
    D : data file containing statistics to be plot (xarray)
    show_dev : whether to show variability around the 
               climatology mean (bool)
    sim_name : plot title (str)
    '''
    if metprm == 'pr' :
        color_map = 'PuOr'
        variable_name = 'Precipitation '
        scatter_name = 'Ensemble Mean Percent Change (%)'
        contour_name = 'Ensemble Standard Deviation (%)'
        scatter_range =[-20,20]
        contour_upper=np.arange(0,45,5)
        D['mean_diff']=D.rel_diff
    elif metprm =='tasmax':
        color_map = 'RdYlBu_r'
        scatter_name = 'Ensemble Mean Change'
        variable_name = 'Maximum Air Temperature '
        contour_name = 'Ensemble Standard Deviation'
        D['mean_diff']=D.raw_diff/100
        print(D.dev_diff.max().values)
        D['dev_diff']=D.dev_diff/100
        print(D.dev_diff.max().values)
        scatter_range =[-2,2]
        contour_upper=np.arange(0,1.6,0.2)
    elif metprm == 'sfcWindmax':
        color_map = 'RdYlBu_r'
        variable_name = 'Maximum Wind Speed '
        scatter_name = 'Ensemble Mean Percent Change (%)'
        contour_name = 'Ensemble Standard Deviation (%)'
        scatter_range =[-3,3]
        contour_upper=np.arange(0,9,1)
        D['mean_diff']=D.rel_diff
    elif metprm == 'sfcWind':
        color_map = 'RdYlBu_r'
        variable_name = 'Mean Wind Speed '
        scatter_name = 'Ensemble Mean Percent Change (%)'
        contour_name = 'Ensemble Standard Deviation (%)'
        scatter_range =[-3,3]
        contour_upper=np.arange(0,9,1)
        D['mean_diff']=D.rel_diff
    elif metprm == 'clt':
        color_map = 'RdYlBu_r'
        variable_name = 'Total Cloud Fraction '
        scatter_name = 'Ensemble Mean Percent Change (%)'
        contour_name = 'Ensemble Standard Deviation (%)'
        scatter_range =[-6,6]
        contour_upper=np.arange(0,7,1)
        D['mean_diff']=D.rel_diff
    elif metprm == 'rsds':
        color_map = 'RdYlBu_r'
        variable_name = 'Surface Solar Radiation '
        scatter_name = 'Ensemble Mean Percent Change (%)'
        contour_name = 'Ensemble Standard Deviation (%)'
        scatter_range =[-6,6]
        contour_lower=np.arange(-8,1,1)
        contour_upper=np.arange(0,9,1)
        D['mean_diff']=D.rel_diff
    else:
        print('Do not include this parameter')

    min_lat =  latlons[0] #D.lat.values.min()
    max_lat =  latlons[1] #D.lat.values.max()
    min_lon =  latlons[2] #D.lon.values.min()
    max_lon =  latlons[3] #D.lon.values.max()
    clon = min_lon + (max_lon - min_lon)/2.0
    clat = min_lat + (max_lat - min_lat)/2.0 
    month_lab=['January','February', 'March','April',
               'May','June','July','August',
               'September','October','November','December']

    
    month = range(1,13)    
    print(variable_name,contour_name,scatter_name)
    fig,axs = plt.subplots(            # add 12 subplots and remove axs
        frameon=False, nrows=3,
        ncols=4,figsize=(28, 18),
        subplot_kw={'xticks': [], 'yticks': []}) 

    fig.subplots_adjust(               # adjust subplots positions and spaces
        left=0.01, right=0.90,
        top =0.95, bottom=0.05,
        hspace=0.06, wspace=0.05) 

    for i in range (0,3):              # deleted the edge line of subpolts
        for j in range (0,4):
            axs[i,j].spines['left'].set_visible(False)
            axs[i,j].spines['right'].set_visible(False)
            axs[i,j].spines['bottom'].set_visible(False)
            axs[i,j].spines['top'].set_visible(False)
    
    for i in range(0,12):
        chart = fig.add_subplot(                       # add projection map
            3, 4, i+1,projection=cartopy.crs.LambertConformal(
                central_longitude=clon,central_latitude=clat))

        _ = chart.set_extent(
            [min_lon, max_lon, min_lat, max_lat], 
            crs=cartopy.crs.PlateCarree())

        _ = chart.add_image(stamen_terrain,8,interpolation='spline16')

        m0 = chart.scatter(                  # add scatter plot with relative difference (color) and significant SD level (size) 
            D.lon,D.lat,
            transform=cartopy.crs.PlateCarree(),
            cmap=color_map,    
            c=D.mean_diff.sel(month=month[i])*100,
            s=D.sig_diff.sel(month=month[i])*5,    
            vmin=min(scatter_range),vmax=max(scatter_range))  
   

        if show_dev : 
            
            rd = D.dev_diff.sel(month=month[i])*100

            m1 = rd.plot.contour(
                'lon','lat',ax=chart,
                levels=contour_upper,
                linewidths=4,linestyles='solid',
                transform=cartopy.crs.PlateCarree(),
                add_colorbar=False,
                vmin=0,vmax=min(scatter_range),
                alpha=0.9,cmap='Greys')

        _ = plt.title(month_lab[i],fontsize=14)

    if show_dev :
        #add contour lines colorbar

        bounds=contour_upper
        cmap=plt.get_cmap('Greys',len(bounds))
        norm=mpl.colors.BoundaryNorm(bounds,len(bounds))
        ax_coutour=fig.add_axes([0.96, 0.05, 0.015, 0.9])
        mpl.colorbar.ColorbarBase(ax_coutour,cmap = cmap,
            norm=norm)


    plt.figtext(0.99,0.6,variable_name+contour_name,fontsize=10,rotation='vertical')
    fig.suptitle(variable_name+sim_name, fontsize=16)
    plt.colorbar(
        m0, cax = plt.axes([0.91, 0.05, 0.015, 0.9]),
        label=variable_name+scatter_name)
    plt.savefig(variable_name+sim_name+'.png')
    print (variable_name+sim_name+'.png')
#     plt.show()


    

# ________________________________________________________________________
# ::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::

''' Pull data
    ---------
    Data paths and file names will vary. 
                                                                       '''
datapath = '/home/ubuntu/eucordex/NorthSea'
#datapath = '/home/tristan/Projekte/WoodPLC/CleanEnergySketchbook/data'
#datapath = '/home/zheng'
''' Different met-parameters will have different associated aggregations. 
    * May want to edit so that aggregation is set based on a seperate 
      external argument, since in future we might have multiple 
      aggregations performed on the same parameters.

                                                                       '''
fname_hsim=[]
fname_psim=[]

matrix_channel=[[0,0,0,0]]
matrix_stats=[[0,0,0,0]] #make an empty matrix


#metprm = 'pr'
if metprm == 'pr' :
    aggr = 'monsum_'
elif metprm =='tasmax':
    aggr = 'monmax_'
elif metprm == 'rsds':
    aggr = 'monmean_'
elif metprm== 'clt':
    aggr = 'monmean_'
elif metprm =='sfcWindmax':
    aggr = ''
elif metprm =='sfcWind':
    aggr = 'monmean_'
else :
    aggr = ''
    
fname_hsim = grod.glob(
    datapath + '/'
    + aggr + 'NorthSea_'
    + metprm+ '_EUR-11_*_historical_*.nc')
fname_psim = grod.glob(
    datapath + '/'
    + aggr + 'NorthSea_'
    + metprm+ '_EUR-11_*_rcp85_*.nc')


# -> WILL NEED MORE SECURE WAY TO MATCH HIST/PROJ SIMS <- # 
# made in set_values_function as a if statement#
fname_hsim.sort()
fname_psim.sort()
if metprm == 'sfcWindmax':
    headers_h= np.array([('_'.join(x.split('_')[1:4])+'_'.join(x.split('_')[6:])) 
        for x in      fname_hsim])
    headers_p= np.array([('_'.join(x.split('_')[1:4])+'_'.join(x.split('_')[6:])) 
        for x in fname_psim])
else:
    headers_h= np.array([('_'.join(x.split('_')[2:5])+'_'.join(x.split('_')[7:])) 
        for x in      fname_hsim])
    headers_p= np.array([('_'.join(x.split('_')[2:5])+'_'.join(x.split('_')[7:])) 
        for x in fname_psim])
'''
    Basic Stats
    -----------
    Collect an ensmeble of outputs and estimates some simple stats

'''
graph_range=[50.0,54.0,-2.0,6.0] #input consequence: min_lat,max_lat,min_lon,max_lon
ens = []                        # create list of datasets
dlist = []
ix = 0
while ix < len(fname_hsim) :
    if not headers_h[ix] == headers_p[ix]:
        print( "++ unmatched history and project data ++")
        break
    D = set_values(
        hsim_path=fname_hsim[ix],
        psim_path=fname_psim[ix],
        metprm=metprm)
    x = list(D.data_vars)            # for now consider only relative diff and raw diff
    x.remove('rel_diff')
    x.remove('raw_diff')
    X = D.drop(x)
    if 'height' in list(X.coords) :
        X=X.drop('height')
    dlist += [X]
    ens += [regrid(X,'nn',datapath+'/'+'base_grid.txt')]   # use common grid
    del(X)
    ix += 1

    
E = ens[0].copy(deep=True)
E.raw_diff.values = np.nanmean(
    np.array([X.raw_diff.values for X in ens]),axis=0)
E.rel_diff.values = np.nanmean(
        np.array([X.rel_diff.values for X in ens]),axis=0)
if metprm =='tasmax':
    E['dev_diff'] = E.raw_diff.copy(deep=True)
    E.dev_diff.values = np.nanstd(
        np.array([X.raw_diff.values for X in ens]),axis=0)
    E['err_diff'] =  E.dev_diff/np.sqrt(len(ens))
    E['sig_diff'] = E.err_diff.copy(deep=True)
    E['sig_diff'].values = (E.sig_diff.values*0)+4  
    for m in range(1,13) : 
        for sig_lev in range(4,0,-1) : 
            E['sig_diff'].sel(month=m).values[
                np.abs(E.raw_diff.sel(month=m)) <= sig_lev*E.err_diff.sel(month=m)
            ] = sig_lev
else:
    E['dev_diff'] = E.rel_diff.copy(deep=True)
    E.dev_diff.values = np.nanstd(
        np.array([X.rel_diff.values for X in ens]),axis=0)
    E['err_diff'] =  E.dev_diff/np.sqrt(len(ens))
    E['sig_diff'] = E.err_diff.copy(deep=True)
    E['sig_diff'].values = (E.sig_diff.values*0)+4  
    for m in range(1,13) : 
        for sig_lev in range(4,0,-1) : 
            E['sig_diff'].sel(month=m).values[
                np.abs(E.rel_diff.sel(month=m)) <= sig_lev*E.err_diff.sel(month=m)
            ] = sig_lev


show_range(E,graph_range,metprm) 
map_values(E,graph_range,metprm,show_dev=True, sim_name='Ensemble')


# ==:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:==

# ////////////////////////////////////////////////////////////////////////
# CDO version of creating ensemble means
# for ...          
    # Dr = remap(X,'nn',datapath+'/'+'base_grid.txt')
    # Dr.to_netcdf('ensmemb'+str(ix).zfill(2)+'.nc')
    # ensmembs += ['ensmemb'+str(ix).zfill(2)+'.nc']

# cdo.ensmean(
#     input=ensmembs,
#     output='ensmean.nc')
# [os.remove(x) for x in ensmembs]

# ////////////////////////////////////////////////////////////////////////
